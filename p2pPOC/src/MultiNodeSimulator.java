import FogLayer.Message.Header.Header;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import FogLayer.Node;
import SharedDataStructures.GroupTable.GroupTable;
import jdk.nashorn.internal.ir.debug.ObjectSizeCalculator;

import java.lang.instrument.Instrumentation;
import java.util.Scanner;

/**
 * Dev: mmachado on 23/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class MultiNodeSimulator {

    private static Scanner standardInput;

    public static void main(String[] args) {

        assert args.length == 0 || args.length == 2 : "Invalid number of arguments (ID && IP || N/A)";

        Node[] arrNode = new Node[4];

        arrNode[0] = new Node(0, "localhost:3000");
        arrNode[1] = new Node(1, "localhost:3001");
        arrNode[2] = new Node(2, "localhost:3002");
        arrNode[3] = new Node(3, "localhost:3003");
        standardInput = new Scanner(System.in);

        boolean exit = false;
        do {
            System.out.println(printOption());
            System.out.print("Please choose [1 - 4]: ");
            switch (standardInput.nextInt()) {
                case 1:
                    for(Node aNode : arrNode){
                        aNode.lookForGroup();
                    }
                    break;
                case 2:
                    break;
                case 3:
                    System.out.print("\033[H\033[2J");
                    System.out.flush();
                    for(Node aNode : arrNode)
                        System.out.println(aNode.reportStatus());

                    break;
                case 4:
                    exit = true;
                    break;
                default:
                    System.out.println("Invalid input, ignoring request.");
                    break;
            }
        } while (!exit);
        System.exit(1);
    }

    private static String printOption() {
        return "1 - Look for group\n" +
                "(DEPRECATED)2 - Leave current group\n" +
                "3 - Report status\n" +
                "4 - Exit program.";
    }

}
