package SharedDataStructures;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Dev: mmachado on 04/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
@SuppressWarnings("Duplicates")
public class SharedHashMap<K, V> extends HashMap<K, V> implements Map<K, V> {

    private volatile boolean isLocked;


    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public SharedHashMap() {
        super();
        this.isLocked = false;
    }

    /**
     * Returns the number of key-value mappings in this map.
     *
     * @return the number of key-value mappings in this map
     */
    @Override
    public synchronized int size() {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        int tmp = super.size();
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Returns <tt>true</tt> if this map contains no key-value mappings.
     *
     * @return <tt>true</tt> if this map contains no key-value mappings
     */
    @Override
    public synchronized boolean isEmpty() {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        boolean tmp = super.isEmpty();
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if this map contains no mapping for the key.
     * <p>
     * <p>More formally, if this map contains a mapping from a key
     * {@code k} to a value {@code v} such that {@code (key==null ? k==null :
     * key.equals(k))}, then this method returns {@code v}; otherwise
     * it returns {@code null}.  (There can be at most one such mapping.)
     * <p>
     * <p>A return value of {@code null} does not <i>necessarily</i>
     * indicate that the map contains no mapping for the key; it's also
     * possible that the map explicitly maps the key to {@code null}.
     * The {@link #containsKey containsKey} operation may be used to
     * distinguish these two cases.
     *
     * @param key the key whose value will be returned
     * @see #put(Object, Object)
     */
    @Override
    public synchronized V get(Object key) {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        V tmp = super.get(key);
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Returns <tt>true</tt> if this map contains a mapping for the
     * specified key.
     *
     * @param key The key whose presence in this map is to be tested
     * @return <tt>true</tt> if this map contains a mapping for the specified
     * key.
     */
    @Override
    public synchronized boolean containsKey(Object key) {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        boolean tmp = super.containsKey(key);
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key, the old
     * value is replaced.
     *
     * @param key   key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>.
     * (A <tt>null</tt> return can also indicate that the map
     * previously associated <tt>null</tt> with <tt>key</tt>.)
     */
    @Override
    public synchronized V put(K key, V value) {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        V tmp = super.put(key, value);
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Removes the mapping for the specified key from this map if present.
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>.
     * (A <tt>null</tt> return can also indicate that the map
     * previously associated <tt>null</tt> with <tt>key</tt>.)
     */
    @Override
    public synchronized V remove(Object key) {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        V tmp = super.remove(key);
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Removes all of the mappings from this map.
     * The map will be empty after this call returns.
     */
    @Override
    public synchronized void clear() {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        super.clear();
        isLocked = false;

        notifyAll();
    }

    /**
     * Returns a {@link Set} view of the keys contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own <tt>remove</tt> operation), the results of
     * the iteration are undefined.  The set supports element removal,
     * which removes the corresponding mapping from the map, via the
     * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>,
     * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
     * operations.  It does not support the <tt>add</tt> or <tt>addAll</tt>
     * operations.
     *
     * @return a set view of the keys contained in this map
     */
    @Override
    public synchronized Set<K> keySet() {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        Set<K> tmp = super.keySet();
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Returns a {@link Collection} view of the values contained in this map.
     * The collection is backed by the map, so changes to the map are
     * reflected in the collection, and vice-versa.  If the map is
     * modified while an iteration over the collection is in progress
     * (except through the iterator's own <tt>remove</tt> operation),
     * the results of the iteration are undefined.  The collection
     * supports element removal, which removes the corresponding
     * mapping from the map, via the <tt>Iterator.remove</tt>,
     * <tt>Collection.remove</tt>, <tt>removeAll</tt>,
     * <tt>retainAll</tt> and <tt>clear</tt> operations.  It does not
     * support the <tt>add</tt> or <tt>addAll</tt> operations.
     *
     * @return a view of the values contained in this map
     */
    @Override
    public synchronized Collection<V> values() {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        Collection<V> tmp = super.values();
        isLocked = false;

        notifyAll();

        return tmp;
    }

    /**
     * Returns a string representation of this map.  The string representation
     * consists of a list of key-value mappings in the order returned by the
     * map's <tt>entrySet</tt> view's iterator, enclosed in braces
     * (<tt>"{}"</tt>).  Adjacent mappings are separated by the characters
     * <tt>", "</tt> (comma and space).  Each key-value mapping is rendered as
     * the key followed by an equals sign (<tt>"="</tt>) followed by the
     * associated value.  Keys and values are converted to strings as by
     * {@link String#valueOf(Object)}.
     *
     * @return a string representation of this map
     */
    @Override
    public synchronized String toString() {
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
                System.err.println(e.getCause().toString());
                System.exit(1);
            }
        }
        isLocked = true;
        String tmp = super.toString();
        isLocked = false;

        notifyAll();

        return tmp;
    }
}
