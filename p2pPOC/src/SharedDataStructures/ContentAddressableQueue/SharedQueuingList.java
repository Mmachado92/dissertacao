package SharedDataStructures.ContentAddressableQueue;

import FogLayer.Message.EmptyMessage;
import FogLayer.Message.MessageInterface;

import java.util.*;

/**
 * Dev: mmachado on 20/11/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class SharedQueuingList<K> {

    private volatile boolean isLocked;

    /**
     * Constructs an empty linked list.
     */

    private LinkedList<K> queue ;
    private volatile boolean isEmpty;
    private volatile int size;

    public SharedQueuingList() {
        queue = new LinkedList<>();
        isLocked = false;
        isEmpty = true;
    }

    /**
     * Appends the specified element to the end of this list.
     * <p>
     * <p>This method is equivalent to addLast.
     *
     * @param o element to be appended to this list
     * @return {@code true})
     */
    public synchronized boolean add(K o) {

        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isLocked = true;
        boolean tmp = queue.add(o);
        size++;
        isEmpty = false;
        isLocked = false;
        notifyAll();

        return tmp;
    }

    /**
     * Retrieves and removes the head (first element) of this list.
     *
     * @return the head of this list, or {@code null} if this list is empty
     */
    public synchronized K poll() {

        while(isLocked || isEmpty){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isLocked = true;
        K tmp =  queue.poll();
        isEmpty = queue.isEmpty();
        size--;
        isLocked = false;
        notifyAll();

        assert tmp != null : "Queue retrieved empty message!";

        return tmp;
    }

    public synchronized boolean isEmpty(){
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        isLocked = true;
        boolean tmp = queue.isEmpty();
        isLocked = false;
        notifyAll();

        return tmp;
    }


    @SuppressWarnings("Duplicates")
    public synchronized boolean contains(String source) {

        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isLocked = true;

        if(isEmpty){
            isLocked = false;
            notifyAll();
            return false;
        }

        for (K aQueue : queue) {
            MessageInterface msg_i = (MessageInterface) aQueue;
            if (msg_i.getHeader().getSource().equals(source)) {
                isLocked = false;
                notifyAll();
                return true;
            }
        }
        isLocked = false;
        notifyAll();

        return false;
    }


    @SuppressWarnings("Duplicates")
    public synchronized MessageInterface findBySource(String source){

        while(isLocked || isEmpty){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        isLocked = true;
        for (K aQueue : queue) {
            MessageInterface msg_i = (MessageInterface) aQueue;
            if (msg_i.getHeader().getSource().equals(source)) {
                queue.remove(aQueue);
                size--;
                isEmpty = queue.isEmpty();
                isLocked = false;
                notifyAll();
                return msg_i;
            }
        }
        isLocked = false;
        notifyAll();

        return new EmptyMessage();
    }

    public synchronized void clear() {
        assert !isEmpty : "Attempting to clear an empty structure";
        while(isLocked){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        isLocked = true;
        queue.clear();
        size = queue.size();
        isEmpty = true;
        isLocked = false;
        notifyAll();
    }

    public int size() {
        return size;
    }
}
