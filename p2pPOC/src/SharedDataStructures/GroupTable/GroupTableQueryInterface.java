package SharedDataStructures.GroupTable;

/**
 * Dev: mmachado on 04/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface GroupTableQueryInterface {

    boolean hasEntry(String ip);

    String[] getIpAddresses();

    String getLeaderIP();
}