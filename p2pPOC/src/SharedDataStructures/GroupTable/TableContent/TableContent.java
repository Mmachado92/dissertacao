package SharedDataStructures.GroupTable.TableContent;

import java.io.Serializable;

/**
 * Created by mmachado on 23/10/16.
 * Organization: Universidade de Aveiro.
 * Subject: P2P Proof of Concept.
 * Contact: miguel.cbm@ua.pt
 */
public class TableContent implements Serializable{
    private String ipAddress;
    private boolean isActive, isLeader;

    public TableContent(String ip, boolean active, boolean leader) {
        this.ipAddress = ip;
        this.isActive = active;
        this.isLeader = leader;
    }

    /*Gets*/
    public String getIpAddress() {
        return ipAddress;
    }
    public boolean isActive() {
        return isActive;
    }
    public boolean isLeader() {
        return isLeader;
    }


    /*Sets*/
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
    public void setActive(boolean active) {
        isActive = active;
    }
    public void setLeader(boolean leader) { isLeader = leader; }
    @Override
    public String toString() {
        return "TableContent{" +
                "ipAddress='" + ipAddress + '\'' +
                ", isActive=" + isActive +
                ", isLeader=" + isLeader +
                '}';
    }

}
