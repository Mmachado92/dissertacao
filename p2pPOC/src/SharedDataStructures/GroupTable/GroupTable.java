package SharedDataStructures.GroupTable;

/*Java Packages*/

import SharedDataStructures.GroupTable.TableContent.TableContent;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * Created by mmachado on 23/10/16.
 * Organization: Universidade de Aveiro.
 * Subject: P2P Proof of Concept.
 * Contact: miguel.cbm@ua.pt
 */
@SuppressWarnings("Duplicates")
public class GroupTable<K, V> implements Serializable {

    private Map<K, V> hashMap;

    public GroupTable() {
        hashMap = new HashMap<>();
    }

    public boolean hasEntry(String ip) {
        for (Object o : hashMap.values()) {
            TableContent tc = (TableContent) o;
            if (tc.getIpAddress().equals(ip)) return true;
        }
        return false;
    }

    public String[] getIpAddresses() {

        String[] ipAddresses = new String[hashMap.size()];
        Iterator itVal = hashMap.values().iterator();
        int i = 0;
        while (itVal.hasNext()) {
            TableContent tc = (TableContent) itVal.next();
            ipAddresses[i++] = tc.getIpAddress();
        }

        return ipAddresses;
    }

    /**
     * Returns the number of key-value mappings in this map.  If the
     * map contains more than <tt>Integer.MAX_VALUE</tt> elements, returns
     * <tt>Integer.MAX_VALUE</tt>.
     *
     * @return the number of key-value mappings in this map
     */
    public int size() {

        return hashMap.size();
    }

    /**
     * Returns <tt>true</tt> if this map contains no key-value mappings.
     *
     * @return <tt>true</tt> if this map contains no key-value mappings
     */
    public boolean isEmpty() {

        return hashMap.isEmpty();
    }


    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if this map contains no mapping for the key.
     * <p>
     * <p>More formally, if this map contains a mapping from a key
     * {@code k} to a value {@code v} such that {@code (key==null ? k==null :
     * key.equals(k))}, then this method returns {@code v}; otherwise
     * it returns {@code null}.  (There can be at most one such mapping.)
     * <p>
     * <p>If this map permits null values, then a return value of
     * {@code null} does not <i>necessarily</i> indicate that the map
     * contains no mapping for the key; it's also possible that the map
     * explicitly maps the key to {@code null}.  The containsKey operation
     * may be used to distinguish these two cases.
     *
     * @param key the key whose associated value is to be returned
     * @return the value to which the specified key is mapped, or
     * {@code null} if this map contains no mapping for the key
     * @throws ClassCastException   if the key is of an inappropriate type for
     *                              this map
     *                              (<a href="{@docRoot}/java/util/Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified key is null and this map
     *                              does not permit null keys
     *                              (<a href="{@docRoot}/java/util/Collection.html#optional-restrictions">optional</a>)
     */
    public V get(K key) {

        return hashMap.get(key);
    }


    /**
     * Associates the specified value with the specified key in this map
     * (optional operation).  If the map previously contained a mapping for
     * the key, the old value is replaced by the specified value.  (A map
     * <tt>m</tt> is said to contain a mapping for a key <tt>k</tt> if and only
     * if m.containsKey(k)} would return
     * <tt>true</tt>.)
     *
     * @param key   key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @throws UnsupportedOperationException if the <tt>put</tt> operation
     *                                       is not supported by this map
     * @throws ClassCastException            if the class of the specified key or value
     *                                       prevents it from being stored in this map
     * @throws NullPointerException          if the specified key or value is null
     *                                       and this map does not permit null keys or values
     * @throws IllegalArgumentException      if some property of the specified key
     *                                       or value prevents it from being stored in this map
     */
    public void put(K key, V value) {
        hashMap.put(key, value);
    }


    /**
     * Removes the mapping for a key from this map if it is present
     * (optional operation).   More formally, if this map contains a mapping
     * from key <tt>k</tt> to value <tt>v</tt> such that
     * <code>(key==null ?  k==null : key.equals(k))</code>, that mapping
     * is removed.  (The map can contain at most one such mapping.)
     * <p>
     * <p>Returns the value to which this map previously associated the key,
     * or <tt>null</tt> if the map contained no mapping for the key.
     * <p>
     * <p>If this map permits null values, then a return value of
     * <tt>null</tt> does not <i>necessarily</i> indicate that the map
     * contained no mapping for the key; it's also possible that the map
     * explicitly mapped the key to <tt>null</tt>.
     * <p>
     * <p>The map will not contain a mapping for the specified key once the
     * call returns.
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>.
     * @throws ClassCastException   if the key is of an inappropriate type for
     *                              this map
     *                              (<a href="{@docRoot}/java/util/Collection.html#optional-restrictions">optional</a>)
     * @throws NullPointerException if the specified key is null and this
     *                              map does not permit null keys
     *                              (<a href="{@docRoot}/java/util/Collection.html#optional-restrictions">optional</a>)
     */
    public V remove(K key) {

        return hashMap.remove(key);

    }

    /**
     * Removes all of the mappings from this map (optional operation).
     * The map will be empty after this call returns.
     */
    public void clear() {

        hashMap.clear();

    }

    @Override
    public String toString() {
        String tableDescription = "#Group Table#\n";
        Iterator entries = hashMap.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            Integer key = (Integer) thisEntry.getKey();
            TableContent tc = (TableContent) thisEntry.getValue();
            tableDescription += "ID: " + key +
                    "\t| IP: " + tc.getIpAddress() +
                    "\t| isActive: " + tc.isActive() +
                    "\t| isLeader: " + tc.isLeader()+"\n";
        }


        return tableDescription;
    }

    /**
     * Removes the given IP address from the table.
     *
     * @param source removes all mappings related to this IP.
     */
    @SuppressWarnings("unchecked")
    public void removeByIPAddress(String source) {

        Iterator entries = hashMap.entrySet().iterator();
        boolean removed = false;
        while (entries.hasNext() && !removed) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            K key = (K) thisEntry.getKey();
            TableContent tc = (TableContent) thisEntry.getValue();

            if (tc.getIpAddress().equals(source)) {
                remove(key);
                removed = true;
            }
        }
    }

    public String getLeaderIP() throws NoSuchElementException {

        Iterator entries = hashMap.entrySet().iterator();
        TableContent tc;
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            tc = (TableContent) thisEntry.getValue();
            if (tc.isLeader())
                return tc.getIpAddress();
        }

        throw new NoSuchElementException();
    }

    public int getLowestID() {

        int lowestID = Integer.MAX_VALUE;

        for (Map.Entry thisEntry : hashMap.entrySet()) {
            lowestID = lowestID > (int) thisEntry.getKey() ? (int) thisEntry.getKey() : lowestID;
        }

        return lowestID;
    }

    /**
     * Removes the current group leader in this table.
     * The group table leader will be lost after this operation. To maintain invariance
     * within a group a new leader must be inserted.
     */
    @SuppressWarnings("unchecked")
    public void removeLeader() {
        Iterator entries = hashMap.entrySet().iterator();
        boolean removed = false;
        while (entries.hasNext() && !removed) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            K key = (K) thisEntry.getKey();
            TableContent tc = (TableContent) thisEntry.getValue();

            if (tc.isLeader()) {
                remove(key);
                removed = true;
            }
        }
    }

    public int getLeaderID() {

        Iterator entries = hashMap.entrySet().iterator();
        TableContent tc;
        while (entries.hasNext()) {
            Map.Entry thisEntry = (Map.Entry) entries.next();
            tc = (TableContent) thisEntry.getValue();
            if (tc.isLeader())
                return (int) thisEntry.getKey();
        }

        return -1;
    }

}
