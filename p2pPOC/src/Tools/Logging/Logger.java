package Tools.Logging;

import java.io.*;

/**
 * Dev: mmachado on 07/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Logger {

    private File logFile;

    public Logger(String filename) {
        logFile = new File(File.pathSeparator + filename + "_log.txt");
    }

    public void writeTo(File filename, String content){
        //TODO: Access file and log info into it.
    }

    public void write(String info) {
        try (FileWriter fw = new FileWriter(logFile, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(toString());
        } catch (IOException e) {
            System.err.println("Caught an In/Output exception whilst trying to write to logging file.");

        }
    }

}
