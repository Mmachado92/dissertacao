package Tools;

import java.io.IOException;
import java.io.Serializable;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.System.err;

/**
 * Dev: mmachado on 20/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Configurations implements Serializable {

    /**
     * Thread-related Configuration GLOBAL variables
     */
    public static final int MAX_KEEPALIVE_TRIES = 3;

    public static final byte MAX_WAIT_TRY = 5;



    public static final short MAX_WAIT_TIME = 2500;

    public static final int MAX_PERIODIC_TIME_INCREMENT = 500;

    public static final int FAULT_DUPLICATE_THREASHOLD = 0; //Duplicate

    public static final int FAULT_LOSSY_THREASHOLD = -1; //Loss

    public static final int FAULT_MAXIMUM_THREASHOLD = 11;  //exclusive value
    public static final int DATAGRAM_PACKET_SIZE = 1024;

    public static final int PLUG_DATAGRAM_PACKET_SIZE = 64;
    public static final int DEFAULT_GW_COM_PORT = 3000;

    //VOLATILE!!!
    public static final String CLOUD_ADDRESS = "192.168.250.144";       //fe80::3204:e1e6:ad65:7a09

    /**
     * Networking Function. Provides a random available port upon invocation.
     * @return A pseudo-random port.
     * @throws IOException If no such port is available.
     */
    public static Integer getRandomAvailablePortNumber() throws IOException {
        return new ServerSocket(0).getLocalPort();
    }

    public static InetAddress getEthernetAddress(){
        String interfaceName = "enp2s0";
        NetworkInterface networkInterface = null;
        try {
            networkInterface = NetworkInterface.getByName(interfaceName);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
        InetAddress currentAddress;
        currentAddress = inetAddress.nextElement();
        while(inetAddress.hasMoreElements())
        {
            currentAddress = inetAddress.nextElement();
            if(/*currentAddress instanceof Inet4Address && */!currentAddress.isLoopbackAddress()) {
                System.out.println(currentAddress.toString());
            }
        }

        return currentAddress;
    }

    public static DatagramSocket getBoundDSocket(int port){
       DatagramSocket ds = null;
        try {
            ds = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
            System.err.println("Couldn't bind socket to port "+port);
            System.exit(1);
        }

        return ds;
    }

    public static DatagramSocket getDSocket(){
        DatagramSocket ds = null;
        try {
            ds = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
            System.err.println("Couldn't bind socket to port");
            System.exit(1);
        }

        return ds;
    }

    public static String getInterfaceIP(String version, String netInterface) {

        // System.out.println("Version:"+"\nContains (ipv4 | ipv6"+version.contains("4")+"|"+version.contains("6"));

        Enumeration<NetworkInterface> nets = null;
        try {
            nets = NetworkInterface.getNetworkInterfaces();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        assert nets != null : "Couldn't obtain given interface IP";
        for (NetworkInterface netint : Collections.list(nets)) {
            if (netInterface.equals(netint.getDisplayName()) || netInterface.equals(netint.getName())) {
                Enumeration<InetAddress> inetAddresses = netint.getInetAddresses();
                String[] adds = new String[10];
                int i = 0;
                for (InetAddress inetAddress : Collections.list(inetAddresses)) {
                    adds[i++] = inetAddress.getHostAddress().split("%")[0];
                }

                if (version.contains("4")) return adds[1];
                else if(version.contains("6")) return adds[0];
            }
        }

        return "localhost";
    }

    public static List<String> getNetworkInterfaces() {
        Enumeration<NetworkInterface> nets = null;
        List<String> interfaceList = new ArrayList<>();

        try {
            nets = NetworkInterface.getNetworkInterfaces();
            interfaceList.addAll(Collections.list(nets).stream().map(NetworkInterface::getDisplayName).collect(Collectors.toList()));

        } catch (SocketException e) {
            err.println("Failed to fetch network interfaces." +
                    "Exception code: " + e.getMessage());
            System.exit(1);
        }

        return interfaceList;
    }
}
