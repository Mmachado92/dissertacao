package Tools.KeepAlive;

/**
 * Dev: mmachado on 04/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface KeepAliveAPI {

    void updateAddressList(String[] addresses);

    void trackAddress(String address);

    void clear();
}
