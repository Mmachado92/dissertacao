package Tools.KeepAlive;

import FogLayer.Alias;
import FogLayer.ControlOps;
import FogLayer.Message.Header.Header;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import FogLayer.Peer.CommunicationManager.CommunicationManager;
import FogLayer.Peer.PeerBehaviours.PeerStates;
import FogLayer.Peer.PeerBehaviours.PeeringLanguage;
import Tools.Configurations;
import SharedDataStructures.SharedHashMap;

/**
 * Dev: mmachado on 01/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class HeartBeatMechanism implements KeepAliveAPI {


    private final String myIP;
    private final CommunicationManager cm_i;
    private SharedHashMap<String, Integer> heartbeat_counter;
    private Thread hb_sender;
    private Thread hb_receiver;

    public HeartBeatMechanism(CommunicationManager cm_i, String myIP) {
        this.myIP = myIP;
        this.cm_i = cm_i;
        this.heartbeat_counter = new SharedHashMap<>();

        hb_sender = new Thread(new HeartBeatSender());
        hb_receiver = new Thread(new HeartBeatReceiver());
        hb_receiver.start();
        hb_sender.start();


        //System.out.println("MY IP IS "+myIP);
    }

    @Override
    public void updateAddressList(String[] addresses) {
        for (String add : addresses)
            heartbeat_counter.put(add, Configurations.MAX_KEEPALIVE_TRIES); //don't skip own address
        //it will induce a bug if the peer is the last one in group
    }

    @Override
    public void trackAddress(String address) {
        heartbeat_counter.put(address, Configurations.MAX_KEEPALIVE_TRIES);
    }

    @Override
    public void clear() {
        heartbeat_counter.clear();
    }


    private class HeartBeatReceiver implements Runnable {


        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {
            while (true) {
                MessageInterface msg_i = cm_i.receiveKeepAliveMessage();
                String heartbeat_source = msg_i.getHeader().getSource();
                //System.out.println("New Heartbeat from "+heartbeat_source);
                if (heartbeat_counter.containsKey(heartbeat_source)) {
                    int value = heartbeat_counter.get(heartbeat_source);//TODO: Fix lines below
                    if (value >= 2) heartbeat_counter.put(heartbeat_source, Configurations.MAX_KEEPALIVE_TRIES);
                    else heartbeat_counter.put(heartbeat_source, value + 1);
                }
            }
        }
    }

    private class HeartBeatSender implements Runnable {


        /**
         * When an object implementing interface <code>Runnable</code> is used
         * to create a thread, starting the thread causes the object's
         * <code>run</code> method to be called in that separately executing
         * thread.
         * <p>
         * The general contract of the method <code>run</code> is that it may
         * take any action whatsoever.
         *
         * @see Thread#run()
         */
        @Override
        public void run() {
            int interim_value;
            while (true) {
                while (heartbeat_counter.isEmpty()) goToSleep();
                heartbeat_counter.remove(myIP);
                String[] addressList = new String[heartbeat_counter.size()];
                heartbeat_counter.keySet().toArray(addressList);
                for (String add : addressList) {
                    //System.out.println("Sending heartbeat to "+add);
                    if (add.equals(myIP)) continue;
                    cm_i.sendKeepAlive(add);
                    if ((interim_value = heartbeat_counter.get(add)) > 0)
                        heartbeat_counter.put(add, (interim_value - 1));
                    else {
                        //System.out.println("Heartbeat: I shall remove thee!"+add);
                        cm_i.sendUnicast(new Message(
                                        new Header(add, myIP),
                                        new Payload<>(PeeringLanguage.PEER_OFFLINE)
                                )
                        );
                        heartbeat_counter.remove(add);
                    }
                }
                goToSleep();

            }
        }
    }

    private void goToSleep() {
        try {
            Thread.sleep(Configurations.MAX_WAIT_TIME);
        } catch (InterruptedException e) {
            System.err.println(e.getMessage());
        }
    }


    @Override
    public String toString() {
        return heartbeat_counter.toString();
    }

}
