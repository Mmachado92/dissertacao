package Tools.KeepAlive;

import FogLayer.Message.MessageInterface;

/**
 * Dev: mmachado on 01/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface KeepAliveMessageBuffer {

    void store(MessageInterface msg_i);

}
