import Cloud.Morpheus;

import java.util.Scanner;


/**
 * Dev: mmachado on 20/04/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class ServerExecutor {


    public static void main(String[] args) {
        Morpheus serverThread = new Morpheus();
        serverThread.start();
        boolean exit = false;
        int option;
        Scanner si = new Scanner(System.in);

        do {
            printMenu();
            option = si.nextInt();
            System.out.print("\033[H\033[2J");
            System.out.flush();
            switch (option) {
                case 1:
                    serverThread.printUniverseStatus();
                    break;
                case 2:
                    System.out.println("Peer ID:");
                    serverThread.peerStatus(si.nextInt());  //notice scanner insice invokation (receiving integer by standard input)
                    break;
                case 99:
                    serverThread.interrupt();
                    exit = true;
                    break;
                default:
                    System.out.println("Unrecognized Option!");
                    break;
            }
        } while (!exit);


    }

    private static void printMenu() {
        System.out.println("Please choose one of the following:");
        System.out.println("1 - Picture the whole universe");
        System.out.println("2 - Peer over peer (required ID)");
        System.out.println("99 - Exit");
        System.out.println("your choice: ");
    }

}
