import FogLayer.Node;
import Tools.Configurations;

import java.util.Scanner;

/**
 * Dev: mmachado on 13/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class UserApplicationInterface {

    private static Scanner standardInput;

    public static void main(String[] args) {

        assert args.length == 0 || args.length == 1 || args.length == 3 : "Invalid number of arguments. Please type java UserApplicationInterface --help to find some guidance.";

        if(args.length==1)
            if(args[0].contains("help") || args[0].contains("h") || args[0].contains("--")){
                printHelp();
                System.exit(0);
            }



        Node aNode;
        String netIntf = "localhost";
        int peerID = 0;
        String ipVersion = "ipv6";

        standardInput = new Scanner(System.in);
        if(args.length == 0) {

            System.out.println("Please, specify the interface: ");
            netIntf = standardInput.next();

            System.out.println("Please, specify the peer's ID: ");
            peerID = standardInput.nextInt();

            System.out.println("Insert the ip version to be used (ipv4 or ipv6):");
            ipVersion = standardInput.next();

        }else if(args.length == 3){
            netIntf = args[0];
            peerID = Integer.valueOf(args[1]);
            ipVersion = args[2];
        }

        String peerIP = Configurations.getInterfaceIP(ipVersion, netIntf);

        aNode = new Node(peerID, peerIP);

        boolean exit = false;
        do{

            System.out.println(printOption());
            System.out.print("Please choose [1 - 2]: ");
            try {
                switch (standardInput.nextInt()) {
                    case 1:
                        System.out.print("\033[H\033[2J");
                        System.out.flush();
                        System.out.println(aNode.reportStatus());
                        break;
                    case 2:
                        exit = true;
                        break;
                    default:
                        System.out.println("Invalid input, ignoring request.");
                        break;
                }
            }catch(Exception e){
                System.err.println("Caught an unhandled exception: "+e.getCause().getMessage()+"\n"+e.getLocalizedMessage());
                System.err.println("Notice: Scanner might have read an invalid character sequence");
                System.exit(1);
            }
        }while(!exit);
        System.exit(1);
    }

    private static void printHelp() {
        System.out.println("This application provides a simple command line interface over the observation of the peer internal status.\n" +
                "Usage: java UserApplicationInterface [interface] [listening port] [ID] [version]" +
                "\n[Interface] - The desired system networking interface whose IP will be captured to be used as main address." +
                "\n\tDisclaimer: Please note that his can be changed " +
                "later on whilst the simulation is running. This will also affect the broadcast between peers." +
                "\nAvailable interfaces: " + "Please enter one of the following: " + Configurations.getNetworkInterfaces().toString() +
                "\n[ID] \t\t- Unique identifier which is used to identify a certain peer within the simulation." +
                "\n[Version]\t\t- The version of the IP desired to be used given the interface");
    }

    private static String printOption() {
        return  "1 - Report status\n" +
                "2 - Exit program.";
    }
}
