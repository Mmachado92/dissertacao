package IOT;

/**
 * Dev: mmachado on 05/04/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public enum RegistryIndicators {
    REGISTRY_STATUS,
    WATTAGE,
    VOLTAGE,
    AMPERES,
}
