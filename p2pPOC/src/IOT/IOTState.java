package IOT;

/**
 * Dev: mmachado on 15/03/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public enum IOTState {
    LOOKING_FOR_MASTER,
    REPORTING
}
