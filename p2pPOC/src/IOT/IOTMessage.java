package IOT;

import FogLayer.Message.Header.Header;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;

import java.io.Serializable;

/**
 * Dev: mmachado on 15/03/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class IOTMessage implements MessageInterface, Serializable{

    private Header h;
    private Payload p;
    private int report;

    public IOTMessage(Header h, Payload p, int report){
        this.h = h;
        this.p = p;
        this.report = report;
    }

    @Override
    public Header getHeader() {
        return h;
    }

    @Override
    public Payload getPayload() {
        return p;
    }

    @Override
    public boolean hasIdentifier() {
        return false;
    }

    @Override
    public int getIdentifier() {
        return -1;
    }

    @Override
    public int getReport(){
        return report;
    }
}
