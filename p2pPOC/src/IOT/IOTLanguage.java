package IOT;

import java.io.Serializable;

/**
 * Dev: mmachado on 15/03/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public enum IOTLanguage implements Serializable {
    ACKNOWLEDGE,
    TRACK_DEVICE,
    MASTER_RESPONSE,
    MASTER_REQUEST,
    REPORTING
}
