package IOT;

import FogLayer.FogPages;
import FogLayer.Message.Header.Header;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;
import Tools.Configurations;

import java.io.*;
import java.net.*;
import java.util.Random;

/**
 * Dev: mmachado on 15/03/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Device extends Thread {

    private final MessageReceiver receiver;
    private String myAddress;
    private String masterAddress;
    private int consumption;
    private IOTState status;
    private SharedQueuingList<MessageInterface> receivedMessages;
    private int myID;


    public Device(String add) {
        this.consumption = 0;
        this.myAddress = add;
        status = IOTState.LOOKING_FOR_MASTER;
        receivedMessages = new SharedQueuingList<>();
        receiver = new MessageReceiver();
        receiver.start();
        myID = new Random().nextInt(100);
    }

    @Override
    public void run() {
        while (true) {

            switch (status) {
                case LOOKING_FOR_MASTER: {
                    MessageInterface response;
                    boolean noResponse = true;
                    while (noResponse) {
                        requestMasterAddress();
                        try {
                            sleep(Configurations.MAX_WAIT_TIME);
                        } catch (InterruptedException e) {
                            System.err.println("Device thread interrupted! Aborting.");
                            e.printStackTrace();
                            System.exit(1);
                        }
                        if (!receivedMessages.isEmpty()) {
                            if ((response = receivedMessages.poll()).getPayload().getContent().equals(IOTLanguage.MASTER_RESPONSE.toString())) {
                                noResponse = false;
                                masterAddress = response.getHeader().getSource();
                                status = IOTState.REPORTING;
                            }
                        }else{System.err.println("no response!");}

                    }
                    break;
                }
                case REPORTING: {
                    boolean proceed = false;
                    int tries = 0;
                    while (!proceed) {
                        report();
                        goToSleep();
                        if (!receivedMessages.isEmpty()){
                            if (receivedMessages.poll().getPayload().getContent().equals(IOTLanguage.ACKNOWLEDGE.toString())) {
                                proceed = true;
                                System.out.println("Acknowledge received");
                            }
                    }else{ System.out.println("No response (no ack)"); }


                        if (tries++ >= 3) {
                            status = IOTState.LOOKING_FOR_MASTER;
                            proceed = true;
                        }
                    }
                    break;
                }
            }
        }
    }

    private void goToSleep() {
        try {
            sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.err.println("Thread malfunction (device). Aborting.");
            System.exit(1);
        }
    }

    private void report() {
        consumption += new Random().nextInt(25);
        DatagramSocket sendSocket = Configurations.getDSocket();
        DatagramPacket outputDatagram;
        byte[] msgByte;


        ByteArrayOutputStream bAux = new ByteArrayOutputStream(Configurations.DATAGRAM_PACKET_SIZE);
        ObjectOutput oAux = null;
        try {
            oAux = new ObjectOutputStream(bAux);

            oAux.writeObject(IOTLanguage.REPORTING.toString());


            oAux.flush();

            msgByte = bAux.toByteArray();

            outputDatagram = new DatagramPacket(msgByte, msgByte.length, InetAddress.getByName(masterAddress), 4000);

            sendSocket.send(outputDatagram);
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Couldn't parse object to ObjectOutput (object)");
        }
    }

    private void requestMasterAddress() {
        DatagramSocket sendSocket = Configurations.getDSocket();
        DatagramPacket outputDatagram;
        byte[] msgByte;
        String ip;
        int port;


        String[] bcastList = { "localhost", "192.168.1.1", "127.0.0.1" };//new FogPages().getBroadcastList();
        for (String add : bcastList) {
            ip = add.substring(0, add.length()-5);
            port = 4000;

            System.out.println("Sending message to: "+ip);

            ByteArrayOutputStream bAux = new ByteArrayOutputStream(Configurations.PLUG_DATAGRAM_PACKET_SIZE);
            ObjectOutput oAux = null;
            try {
                oAux = new ObjectOutputStream(bAux);

                oAux.writeObject(IOTLanguage.MASTER_REQUEST.toString());


                oAux.flush();

                msgByte = bAux.toByteArray();

                outputDatagram = new DatagramPacket(msgByte, msgByte.length, InetAddress.getByName(ip), port);

                sendSocket.send(outputDatagram);
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Couldn't parse object to ObjectOutput (object)");
            }

        }
    }

    @Override
    public String toString() {
        return "Device ID: " + myID + "\n" +
                "Device IP: " + myAddress +"\n"+
                "Status " + status + "\n" +
                "Consumption: " + consumption + " Watts\n" +
                "Master's Address: " + masterAddress;
    }


    private class MessageReceiver extends Thread {

        @Override
        public void run() {
            DatagramSocket receiverSocket = Configurations.getBoundDSocket(61616);
            String msg;
            ObjectInput oAux;
            int packet_size = 0;

            while (true) {

                byte[] buff = new byte[Configurations.PLUG_DATAGRAM_PACKET_SIZE];                 // buffer de recepção
                DatagramPacket inputDatagram = new DatagramPacket(buff, buff.length);

                try {
                    receiverSocket.receive(inputDatagram);
                    byte[] receivedData = new byte[inputDatagram.getLength()];
                    System.arraycopy(inputDatagram.getData(), inputDatagram.getOffset(),
                            receivedData, 0, inputDatagram.getLength());

                    try {
                        ByteArrayInputStream bArr = new ByteArrayInputStream(receivedData);
                        packet_size = bArr.available();
                        oAux = new ObjectInputStream(bArr);
                        msg = String.valueOf(oAux.readObject());

                    } catch (OptionalDataException ode) {
                        System.err.println("Caught an Optional Data Exception while retrieving a message: " + ode.getMessage());
                        System.err.println("Malformed Packet with " + packet_size + " bytes");
                        continue;
                    }

                    receivedMessages.add(new Message(
                            new Header(inputDatagram.getAddress().toString().substring(1), InetAddress.getLocalHost().toString()),
                            new Payload<>(msg))
                    );

                } catch (ClassNotFoundException | IOException e) {
                    e.printStackTrace();
                    receiverSocket.close();
                    System.exit(1);
                }
            }
        }
    }
}