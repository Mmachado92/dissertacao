package FogLayer.Peer.CommunicationManager;

import FogLayer.Alias;
import FogLayer.FogPages;
import FogLayer.Message.Header.Header;
import FogLayer.Message.IdentifierMessage;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import FogLayer.Peer.CommunicationManager.Runnables.*;
import IOT.IOTLanguage;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;

import java.util.Arrays;


/**
 * Created by mmachado on 23/10/16.
 * Organization: Universidade de Aveiro.
 * Subject: P2P Proof of Concept.
 * Contact: miguel.cbm@ua.pt
 */
public class Manager implements CommunicationManager {

    private final String[] broadcastList;
    /**
     * The ip of the Peer_rev1 that this manager serves.
     */
    private String owner_ip;

    /**
     * The port where neighboring peers will connect to.
     */
    private int owner_port;

    /**
     * The data structure which will hold the outgoing messages to
     * neighboring peers.
     * This structure is a map interface with a {@code LinkedHashMap} structure.
     * Note that this is a revamped class inheriting LinkedHashMap and implementing
     * Map interface with some other methods, particular to the concept.
     */
    private SharedQueuingList<MessageInterface> inbox;


    /**
     * The data structure which will hold the outgoing messages to
     * neighboring peers.
     * This structure is a map interface with a {@code LinkedHashMap} structure.
     */
    private SharedQueuingList<MessageInterface> outbox;


    /**
     * The data structure which will hold the outgoing messages to
     * neighboring peers.
     * This structure is a map interface with a {@code LinkedHashMap} structure.
     */
    private SharedQueuingList<MessageInterface> keepAliveMessages;


    private final Thread receiver;

    private final Thread sender;

    private final Thread plugReceiver;


    public Manager(String ip) {
        this.owner_ip = ip;

        this.inbox = new SharedQueuingList<>();
        this.outbox = new SharedQueuingList<>();
        this.keepAliveMessages = new SharedQueuingList<>();

        this.plugReceiver = new Thread(new PlugReceiver(inbox, owner_ip));
        this.receiver = new Thread(new Receiver(keepAliveMessages, inbox));
        this.sender = new Thread(new Sender(outbox));

        this.plugReceiver.setName("PlugReceiver");
        this.receiver.setName("Receiver");
        this.sender.setName("Sender");

        this.plugReceiver.start();
        this.receiver.start();
        this.sender.start();


        this.broadcastList = new FogPages().getBroadcastList(owner_ip);

        //System.out.println("Broadcast LIST:"+ Arrays.toString(broadcastList));
    }

    /**
     *
     */
    @Override
    public void sendBroadcast(Payload p, int id) {

        for (String ip : broadcastList){
            MessageInterface msg_i = new IdentifierMessage(new Header(owner_ip, ip), p, id);
            outbox.add(msg_i);
        }
    }

    @Override
    public MessageInterface receiveKeepAliveMessage(){
        return keepAliveMessages.poll();
    }

    @Override
    public void sendUnicast(MessageInterface msg_i) {
        outbox.add(msg_i);
    }

    @Override
    public void sendKeepAlive(String destination) {
        outbox.add(new Message(
                new Header(owner_ip, destination),
                new Payload<>(Alias.KeepAlive)));
    }

    @Override
    public MessageInterface receiveNextMessage() {
        return inbox.poll();
    }

    @Override
    public MessageInterface receiveMessageByPeer(String peerIP) {
        return inbox.findBySource(peerIP);
    }

    @Override
    public boolean hasNewMessage(){
        return !inbox.isEmpty();
    }

    @Override
    public void clearInbox() {
        inbox.clear();
    }

    @Override
    public String toString(){
        return "Inbox Messages: "+inbox.size() + "\nOutbox Messages: "+outbox.size();
    }
}