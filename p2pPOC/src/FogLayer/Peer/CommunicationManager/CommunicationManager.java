package FogLayer.Peer.CommunicationManager;

import FogLayer.Message.IdentifierMessage;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import IOT.IOTLanguage;

/**
 * Dev: mmachado on 30/10/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface CommunicationManager {

    void sendBroadcast(Payload p, int id);

    MessageInterface receiveKeepAliveMessage();

    void sendUnicast(MessageInterface msg_i);

    void sendKeepAlive(String destination);

    MessageInterface receiveNextMessage();

    MessageInterface receiveMessageByPeer(String peerIP);       //non-blocking

    String toString();

    boolean hasNewMessage();

    void clearInbox();
}