package FogLayer.Peer.CommunicationManager.Runnables;

import FogLayer.Alias;
import FogLayer.Message.MessageInterface;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;
import Tools.Configurations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.Random;

/**
 * Dev: mmachado on 07/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class FaultyReceiver implements Runnable {


    private final SharedQueuingList<MessageInterface> keepAliveMessages;
    private final int owner_port;
    private final SharedQueuingList<MessageInterface> inbox;
    private DatagramSocket receiverSocket;
    private Random rnd_gnr;

    public FaultyReceiver(int owner_port, SharedQueuingList<MessageInterface> keepAliveMessages, SharedQueuingList<MessageInterface> inbox) {
        this.owner_port = owner_port;
        this.keepAliveMessages = keepAliveMessages;
        this.inbox = inbox;
        rnd_gnr = new Random();
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        receiverSocket = Configurations.getBoundDSocket(owner_port);

        while (true) {

            byte[] buff = new byte[2048];                 // buffer de recepção
            DatagramPacket inputDatagram = new DatagramPacket(buff, buff.length);

            try {
                receiverSocket.receive(inputDatagram);
                byte[] receivedData = new byte[inputDatagram.getLength()];
                System.arraycopy(inputDatagram.getData(), inputDatagram.getOffset(),
                        receivedData, 0, inputDatagram.getLength());

                //System.out.println("Peer_rev1 (UDP)" + receivedData.length + " bytes received");

                ObjectInput oAux = new ObjectInputStream(new ByteArrayInputStream(receivedData));
                MessageInterface msg = (MessageInterface) oAux.readObject();


                int bound_hit = rnd_gnr.nextInt(Configurations.FAULT_MAXIMUM_THREASHOLD);
                if (bound_hit >= Configurations.FAULT_DUPLICATE_THREASHOLD)
                    if (msg.getPayload().getContent().equals(Alias.KeepAlive)) keepAliveMessages.add(msg);
                    else inbox.add(msg);
                else if (bound_hit <= Configurations.FAULT_LOSSY_THREASHOLD) continue;

                if (msg.getPayload().getContent().equals(Alias.KeepAlive))
                    keepAliveMessages.add(msg);
                else
                    inbox.add(msg);

                //inbox.add(msg);

            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
                receiverSocket.close();
                System.exit(1);
            }
        }
    }
}
