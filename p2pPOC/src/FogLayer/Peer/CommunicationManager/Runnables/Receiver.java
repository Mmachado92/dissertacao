package FogLayer.Peer.CommunicationManager.Runnables;

import FogLayer.Alias;
import FogLayer.Message.EmptyMessage;
import FogLayer.Message.Header.Header;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;
import Tools.Configurations;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Dev: mmachado on 07/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Receiver implements Runnable {

    private final SharedQueuingList<MessageInterface> keepAliveMessages;
    private final SharedQueuingList<MessageInterface> inbox;


    public Receiver(SharedQueuingList<MessageInterface> keepAliveMessages, SharedQueuingList<MessageInterface> inbox) {
        this.keepAliveMessages = keepAliveMessages;
        this.inbox = inbox;
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        DatagramSocket receiverSocket = Configurations.getBoundDSocket(Configurations.DEFAULT_GW_COM_PORT);
        MessageInterface msg = new EmptyMessage();

        ObjectInput oAux;
        int packet_size = 0;

        while (true) {

            byte[] buff = new byte[Configurations.DATAGRAM_PACKET_SIZE];                 // buffer de recepção
            DatagramPacket inputDatagram = new DatagramPacket(buff, buff.length);

            try {
                receiverSocket.receive(inputDatagram);
                byte[] receivedData = new byte[inputDatagram.getLength()];
                System.arraycopy(inputDatagram.getData(), inputDatagram.getOffset(),
                        receivedData, 0, inputDatagram.getLength());

                //System.out.println("Peer_rev1 (UDP)" + receivedData.length + " bytes received");
                try {
                    ByteArrayInputStream bArr = new ByteArrayInputStream(receivedData);
                    packet_size = bArr.available();
                    oAux = new ObjectInputStream(bArr);
                    msg = (MessageInterface) oAux.readObject();

                } catch (OptionalDataException ode) {
                    System.err.println("Caught an Optional Data Exception while retrieving a message: " + ode.getMessage());
                    System.err.println("Malformed Packet with " + packet_size + " bytes");
                    System.out.println(new String(receivedData));

                    continue;
                } catch (EOFException eofe) {
                    System.err.println("ObjectInputStream exception" + eofe.getMessage());
                    System.out.println(new String(receivedData));
                    //System.out.println(msg.toString());
                }
                if (msg.getPayload().getContent().equals(Alias.KeepAlive))
                    keepAliveMessages.add(msg);
                else
                    inbox.add(msg);

                //inbox.add(msg);

                //System.out.println("Received message:"+msg.toString());

            } catch (ClassNotFoundException | IOException e) {
                e.printStackTrace();
                receiverSocket.close();
                System.exit(1);
            }
        }
    }
}