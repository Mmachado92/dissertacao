package FogLayer.Peer.CommunicationManager.Runnables;

import FogLayer.Message.Header.Header;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;
import Tools.Configurations;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * Dev: mmachado on 07/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */

@SuppressWarnings("Duplicates")
public class PlugReceiver implements Runnable {

    private final SharedQueuingList<MessageInterface> inbox;
    private final int port;
    private String owner_ip;

    public PlugReceiver(SharedQueuingList<MessageInterface> inbox, String owner_ip) {
        this.port = 0xF0B0;      //0xF0B0
        this.inbox = inbox;
        this.owner_ip = owner_ip;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        DatagramSocket receiverSocket = Configurations.getBoundDSocket(port);

        while (true) {

            byte[] buff = new byte[Configurations.PLUG_DATAGRAM_PACKET_SIZE];                 // buffer de recepção
            DatagramPacket inputDatagram = new DatagramPacket(buff, buff.length);

            try {
                receiverSocket.receive(inputDatagram);
                byte[] receivedData = new byte[inputDatagram.getLength()];
                System.arraycopy(inputDatagram.getData(), inputDatagram.getOffset(),
                        receivedData, 0, inputDatagram.getLength());

              /*  try{
                    ByteArrayInputStream bArr = new ByteArrayInputStream(receivedData);
                    deserializedByteArray = new byte[bArr.available()];
                    bArr.read(deserializedByteArray);
                    oAux = new ObjectInputStream(bArr);
                    //msg = (MessageInterface) oAux.readObject();       line belongs to the normal receiver (between gateways)

                }catch(OptionalDataException ode){
                    System.err.println("Caught an Optional Data Exception while retrieving a message: "+ode.getMessage());
                    System.err.println("Malformed Packet with " + deserializedByteArray.length + " bytes");
                    continue;
                }*/


                //System.out.println("Received a new packet with content: "+ oAux.readObject().toString());
                //System.out.println("Packet received from: "+receiverSocket.getRemoteSocketAddress().toString());
                //System.out.println("Received from: "+inputDatagram.getSocketAddress().toString().substring(1));
                //System.out.println("InputDatagram data: "+inputDatagram.getAddress().toString().substring(1));

                String msg = new String(receivedData, "UTF-8");
               // System.out.println("Received: "+msg+"\nFrom: "+inputDatagram.getAddress().toString().substring(1));
                inbox.add(new Message(
                        new Header(inputDatagram.getAddress().toString().substring(1), owner_ip),
                        new Payload<>(msg)));

            } catch (/*ClassNotFoundException | */IOException e) {
                e.printStackTrace();
                receiverSocket.close();
                System.exit(1);
            }
        }
    }
}