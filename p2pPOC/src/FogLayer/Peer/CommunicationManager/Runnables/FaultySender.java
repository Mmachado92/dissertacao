package FogLayer.Peer.CommunicationManager.Runnables;

import FogLayer.Message.MessageInterface;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;
import Tools.Configurations;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;

/**
 * Dev: mmachado on 07/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class FaultySender implements Runnable{

    private final SharedQueuingList<MessageInterface> outbox;
    @SuppressWarnings("Duplicates")
    private
    DatagramSocket sendSocket;

    private Random rnd_gnr = new Random();

    public FaultySender(SharedQueuingList<MessageInterface> outbox){
        this.outbox = outbox;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {

        sendSocket = Configurations.getDSocket();

        while (true) {

            if(!outbox.isEmpty()){
                DatagramPacket outputDatagram = null;
                MessageInterface msg = outbox.poll();
                byte [] msgByte = null;


                String ip = msg.getHeader().getDestination().substring(0, msg.getHeader().getDestination().length()-4);
                int port = Integer.parseInt(msg.getHeader().getDestination().substring(msg.getHeader().getDestination().length()-4), msg.getHeader().getDestination().length());

                try{
                    ByteArrayOutputStream bAux = new ByteArrayOutputStream (2048);
                    ObjectOutput oAux = new ObjectOutputStream(bAux);


                    oAux.writeObject (msg);
                    oAux.flush ();

                    msgByte = bAux.toByteArray ();

                    outputDatagram = new DatagramPacket(msgByte, msgByte.length, InetAddress.getByName(ip), port);

                    /**
                     * Faulty Behaviour
                     */
                    int tmp;
                    if((tmp = rnd_gnr.nextInt(Configurations.FAULT_MAXIMUM_THREASHOLD)) >=
                            Configurations.FAULT_DUPLICATE_THREASHOLD)
                        sendSocket.send(outputDatagram);
                    else if(tmp <= Configurations.FAULT_LOSSY_THREASHOLD) continue;
                    /**
                     * EO Faulty Behaviour
                     */

                    sendSocket.send(outputDatagram);


                } catch (Exception e) {
                    System.err.println(e.getMessage());
                    System.err.println("Could not finish the operation. Check stacktrace for more info.");
                    sendSocket.close();
                    System.exit(1);
                }
            }
        }
    }
}
