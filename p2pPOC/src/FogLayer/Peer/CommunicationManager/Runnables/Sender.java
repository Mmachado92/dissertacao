package FogLayer.Peer.CommunicationManager.Runnables;

import FogLayer.Message.MessageInterface;
import IOT.IOTLanguage;
import SharedDataStructures.ContentAddressableQueue.SharedQueuingList;
import Tools.Configurations;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static java.lang.System.err;

/**
 * Dev: mmachado on 07/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Sender implements Runnable {

    private final SharedQueuingList<MessageInterface> outbox;

    public Sender(SharedQueuingList<MessageInterface> outbox) {
        this.outbox = outbox;
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {


        DatagramSocket sendSocket = Configurations.getDSocket();

        while (true) {
            DatagramPacket outputDatagram;
            byte[] msgByte;
            String ip;
            int port;

            if (!outbox.isEmpty()) {
                MessageInterface msg = outbox.poll();

                try {
                } catch (NullPointerException npe) {
                    err.println(npe.getMessage());
                    err.println("Failed Parsing of Message (toString):" + msg.toString());
                    continue;
                }

                try {
                    ByteArrayOutputStream bAux = new ByteArrayOutputStream(Configurations.DATAGRAM_PACKET_SIZE);

                    if (msg.getPayload().getContent().equals(IOTLanguage.MASTER_RESPONSE) ||
                            msg.getPayload().getContent().equals(IOTLanguage.ACKNOWLEDGE)) {
                        DataOutput dAux = new DataOutputStream(bAux);

                        ip = msg.getHeader().getDestination();
                        String input = msg.getPayload().getContent().toString();
                        byte[] ascii = new byte[input.length()];
                        for (int i = 0; i < input.length(); i++) {
                            char ch = input.charAt(i);
                            ascii[i] = (ch <= 0xFF) ? (byte) ch : (byte) '?';
                        }

                        dAux.write(ascii);
                        port = 61616;
                    } else {
                        ObjectOutput oAux = new ObjectOutputStream(bAux);

                        ip = msg.getHeader().getDestination();
                        port = 3000;
                        oAux.writeObject(msg);
                        oAux.flush();
                    }


                  //  System.out.println("Sending message:"+msg.toString());

                    msgByte = bAux.toByteArray();



                    outputDatagram = new DatagramPacket(msgByte, msgByte.length, InetAddress.getByName(ip), port);

                    sendSocket.send(outputDatagram);

                } catch (Exception e) {
                    e.printStackTrace();
                    err.println(e.getMessage());
                    err.println("Could not finish the operation. Check stacktrace for more info.");
                    sendSocket.close();
                    err.println("Object:" + msg.toString());
                    System.exit(1);
                }
            }
        }
    }
}