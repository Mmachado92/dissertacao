package FogLayer.Peer.PeerBehaviours.Solo;

import FogLayer.Message.Header.Header;
import FogLayer.Message.IdentifierMessage;
import FogLayer.Message.Message;
import FogLayer.Peer.PeerBehaviours.PeerStates;
import FogLayer.Peer.CommunicationManager.CommunicationManager;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import FogLayer.Peer.PeerBehaviours.PeeringLanguage;
import SharedDataStructures.GroupTable.GroupTable;
import SharedDataStructures.GroupTable.TableContent.TableContent;
import Tools.Configurations;

/**
 * Dev: mmachado on 24/10/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Newcomer implements SolitudeBehaviour {

    private int myID;
    private final CommunicationManager cm_i;
    private GroupTable<Integer, TableContent> tmp_gt;
    private int leaderID;
    private String leaderIP, myIP;

    public Newcomer(CommunicationManager cm_i, int myID, String myIP) {
        this.cm_i = cm_i;
        this.tmp_gt = new GroupTable<>();
        this.myID = myID;
        this.myIP = myIP;
        leaderIP = myIP;
        leaderID = myID;
    }

    @Override
    public PeerStates whoIsThere() {


        cm_i.sendBroadcast(new Payload<>(PeeringLanguage.WHO_IS_THERE), myID);

        try {
            Thread.sleep(Configurations.MAX_WAIT_TIME);
        } catch (InterruptedException e) {
            System.err.println("Thread interrupted!");
            System.err.println(e.getCause().toString());
            System.err.println("Exception Message: " + e.getMessage());
            System.exit(1);
        }


        while (cm_i.hasNewMessage()) {
            MessageInterface msg_i = cm_i.receiveNextMessage();
            if (msg_i.getPayload().getContent().equals(PeeringLanguage.ANSWER)) {
                leaderIP = msg_i.getHeader().getSource();
                leaderID = msg_i.getIdentifier();
                return PeerStates.GET_ACQUAINTED;
            } else if (msg_i.getPayload().getContent().equals(PeeringLanguage.START_ELECTION)) {
                return PeerStates.DETACHED;
            }/* else if (msg_i.getPayload().getContent().equals(PeeringLanguage.WHO_IS_THERE)) {
                if (msg_i.getIdentifier() < leaderID) {
                    leaderID = msg_i.getIdentifier();
                    leaderIP = msg_i.getHeader().getSource();
                }
            }*/ //Dis was a bugg, Report it: (peer moved to getAcquainted and the leader wouldn't become aware)
        }

        if (leaderID == myID) return PeerStates.GROUP_LEADER;
        else return PeerStates.GET_ACQUAINTED;
    }

    @Override
    public GroupTable<Integer, TableContent> answerReceived() {

        cm_i.sendUnicast(new IdentifierMessage(
                new Header(myIP, leaderIP),
                new Payload<>(PeeringLanguage.ACKNOWLEDGE),
                myID
        ));

        long t0 = System.currentTimeMillis();
        MessageInterface msg_i;
        while (true) {
            msg_i = cm_i.receiveNextMessage();


            if ((msg_i.getPayload().getContent() instanceof GroupTable) &&
                    msg_i.getIdentifier() == leaderID) {
                return (GroupTable<Integer, TableContent>) msg_i.getPayload().getContent();
            } else if (msg_i.getPayload().getContent().equals(PeeringLanguage.START_ELECTION) ||
                    (msg_i.getPayload().getContent() instanceof GroupTable &&
                            msg_i.getIdentifier() < leaderID))
                return new GroupTable<>();

            System.out.println("Retrying... (answer)");
            cm_i.sendUnicast(new IdentifierMessage(
                    new Header(myIP, leaderIP),
                    new Payload<>(PeeringLanguage.ACKNOWLEDGE),
                    myID
            ));
        }
    }
} //EOF
