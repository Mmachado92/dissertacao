package FogLayer.Peer.PeerBehaviours.Solo;

import FogLayer.Peer.PeerBehaviours.PeerStates;
import SharedDataStructures.GroupTable.GroupTable;
import SharedDataStructures.GroupTable.TableContent.TableContent;

/**
 * Dev: mmachado on 24/10/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface SolitudeBehaviour {

    PeerStates whoIsThere();

    GroupTable<Integer, TableContent> answerReceived();
}
