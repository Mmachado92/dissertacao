package FogLayer.Peer.PeerBehaviours;

import java.io.Serializable;

/**
 * Dev: mmachado on 24/10/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public enum PeerStates implements Serializable{
    DETACHED,
    LOOKING_FOR_GROUP,
    GROUP_LEADER,
    ACQUAINT,
    GET_ACQUAINTED,
    IN_GROUP,
    @Deprecated CREATING_GROUP,
    @Deprecated LEAVE,
    @Deprecated REMOVE_MEMBER,
    @Deprecated ELECTIVE_PROCESS
}
