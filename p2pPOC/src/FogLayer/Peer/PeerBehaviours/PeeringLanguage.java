package FogLayer.Peer.PeerBehaviours;

import java.io.Serializable;

/**
 * Dev: mmachado on 07/03/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public enum PeeringLanguage implements Serializable {
    WHO_IS_THERE,       //broadcast
    ANSWER,             //unicast
    PEER_OFFLINE,       //localhost loopback
    ACKNOWLEDGE ,       //unicast
    START_ELECTION,     //broadcast
}
