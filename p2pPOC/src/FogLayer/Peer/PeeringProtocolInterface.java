package FogLayer.Peer;

/**
 * Dev: mmachado on 12/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface PeeringProtocolInterface {

    void leave();

    void lookForGroup();

    String reportStatus();

    String toString();

}
