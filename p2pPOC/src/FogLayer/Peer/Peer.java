package FogLayer.Peer;

import FogLayer.FogToken;
import FogLayer.Message.Header.Header;
import FogLayer.Message.IdentifierMessage;
import FogLayer.Message.Message;
import FogLayer.Message.MessageInterface;
import FogLayer.Message.Payload.Payload;
import FogLayer.Peer.CommunicationManager.CommunicationManager;
import FogLayer.Peer.PeerBehaviours.PeerStates;
import FogLayer.Peer.PeerBehaviours.PeeringLanguage;
import FogLayer.Peer.PeerBehaviours.Solo.Newcomer;
import FogLayer.Peer.PeerBehaviours.Solo.SolitudeBehaviour;
import IOT.IOTLanguage;
import IOT.RegistryIndicators;
import SharedDataStructures.GroupTable.GroupTable;
import SharedDataStructures.GroupTable.TableContent.TableContent;
import Tools.Configurations;
import Tools.KeepAlive.HeartBeatMechanism;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * Dev: mmachado on 09/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Peer extends Thread {

    private final String myIP;
    private final int myID;
    private boolean isRunning;
    private PeerStates state;
    private CommunicationManager cm_i;
    private SolitudeBehaviour iAmAlone;
    private GroupTable<Integer, TableContent> myGroupTable;
    private HeartBeatMechanism heartBeat;
    private PlugRegistry plugRegistry;
    private Map<String, String> plugTracker;
    private int buffCount;

    public Peer(String myIP, int myID, CommunicationManager cm_i) {
        this.myIP = myIP;
        this.myID = myID;
        isRunning = false;
        state = PeerStates.DETACHED;
        this.cm_i = cm_i;
        this.heartBeat = new HeartBeatMechanism(cm_i, myIP);
        this.myGroupTable = new GroupTable<>();
        this.iAmAlone = new Newcomer(cm_i, myID, myIP);
        this.plugRegistry = new PlugRegistry();
        this.plugTracker = new HashMap<>();
        this.buffCount = 0;
        logStatus();
    }

    @Override
    public void run() {
        isRunning = true;

        //while (!cm_i.receiveNextMessage().getPayload().getContent().equals(ControlOps.JOIN)) ;
        while (isRunning) {

            switch (state) {
                case DETACHED: {
                    state = PeerStates.LOOKING_FOR_GROUP;
                    break;
                }
                case LOOKING_FOR_GROUP: {
                    state = iAmAlone.whoIsThere();
                    if (state == PeerStates.GROUP_LEADER) {
                        myGroupTable.put(myID, new TableContent(myIP, true, true));
                        cm_i.sendBroadcast(new Payload<>(myGroupTable), myID);
                    }
                    //System.out.println("Trainsitioning to" + state.toString());
                    break;
                }
                case GET_ACQUAINTED: {
                    if (!(myGroupTable = iAmAlone.answerReceived()).isEmpty()) {
                        heartBeat.updateAddressList(myGroupTable.getIpAddresses());
                        state = PeerStates.IN_GROUP;
                    } else state = PeerStates.DETACHED;
                    break;
                }
                case IN_GROUP: {
                    System.out.println(toString());
                    MessageInterface received_msg;
                    while (state.equals(PeerStates.IN_GROUP)) {
                        received_msg = cm_i.receiveNextMessage();

                        if (received_msg.getPayload().getContent() instanceof GroupTable &&
                                received_msg.getIdentifier() == myGroupTable.getLeaderID()) {
                            myGroupTable = (GroupTable<Integer, TableContent>) received_msg.getPayload().getContent();
                            heartBeat.updateAddressList(myGroupTable.getIpAddresses());
                        } else if (received_msg.getPayload().getContent().equals(PeeringLanguage.PEER_OFFLINE)) {

                            if (received_msg.getHeader().getSource().equals(myGroupTable.getLeaderIP())) {
                                myGroupTable.removeByIPAddress(received_msg.getHeader().getSource());
                                myGroupTable.put(myGroupTable.getLowestID(), new TableContent(myGroupTable.get(myGroupTable.getLowestID()).getIpAddress(), true, true));
                            } else myGroupTable.removeByIPAddress(received_msg.getHeader().getSource());

                            if (myID == myGroupTable.getLowestID()) {
                                state = PeerStates.GROUP_LEADER;
                                myGroupTable.put(myID, new TableContent(myIP, true, true));
                            }
                            /*IOT is BELOW*/
                        } else if (received_msg.getPayload().getContent().equals(IOTLanguage.TRACK_DEVICE)) {
                            System.out.println("Got new track device request from: " + received_msg.getHeader().getSource());
                            cm_i.sendUnicast(new Message(
                                    new Header(myIP, received_msg.getHeader().getSource()),
                                    new Payload<>(IOTLanguage.MASTER_RESPONSE)));
                        } else if (received_msg.getPayload().getContent().toString().contains(IOTLanguage.REPORTING.toString())) {
                            String[] plugData = received_msg.getPayload().getContent().toString().split(" ");
                            plugDataToRegistry(plugData, received_msg.getHeader().getSource());

                            cm_i.sendUnicast(new Message(
                                    new Header(myIP, received_msg.getHeader().getSource()),
                                    new Payload<>(IOTLanguage.ACKNOWLEDGE)));

                               /**/
                            cm_i.sendUnicast(new Message(
                                    new Header(myIP, Configurations.CLOUD_ADDRESS),
                                    new Payload<>(new FogToken(myID, myIP, state, myGroupTable.getLeaderID(), plugRegistry.size()))
                            ));
                            logStatus();
                        }
                    }
                    break;
                }
                case GROUP_LEADER: {
                    System.out.println(toString());
                    MessageInterface received_msg;
                    while (state.equals(PeerStates.GROUP_LEADER)) {
                        received_msg = cm_i.receiveNextMessage();

                        if (received_msg.getPayload().getContent().equals(PeeringLanguage.WHO_IS_THERE)) {
                            cm_i.sendUnicast(new IdentifierMessage(
                                    new Header(myIP, received_msg.getHeader().getSource()),
                                    new Payload<>(PeeringLanguage.ANSWER),
                                    myID));
                        } else if (received_msg.getPayload().getContent().equals(PeeringLanguage.ACKNOWLEDGE)) {
                            //   System.out.println("sending groupTable to:"+received_msg.getHeader().getSource());
                            myGroupTable.put(received_msg.getIdentifier(), new TableContent(received_msg.getHeader().getSource(), true, false));
                            cm_i.sendBroadcast(new Payload<>(myGroupTable), myID);
                            heartBeat.trackAddress(received_msg.getHeader().getSource());
                        } else if (received_msg.getPayload().getContent() instanceof GroupTable &&
                                received_msg.getIdentifier() < myID) {
                            state = PeerStates.DETACHED;
                        } else if (received_msg.getPayload().getContent().equals(PeeringLanguage.PEER_OFFLINE)) {
                            myGroupTable.removeByIPAddress(received_msg.getHeader().getSource());
                            cm_i.sendBroadcast(new Payload<>(myGroupTable), myID);
                            /*IOT is BELOW*/
                        } else if (received_msg.getPayload().getContent().equals(IOTLanguage.MASTER_REQUEST.toString())) {
                            if (plugTracker.containsKey(received_msg.getHeader().getSource())) {
                                if (plugTracker.get(received_msg.getHeader().getSource()).equals(myIP))
                                /*TODO: Add continue to if clause in order to skip master responses from group leader*/
                                    cm_i.sendUnicast(new Message(
                                            new Header(myIP, received_msg.getHeader().getSource()),
                                            new Payload<>(IOTLanguage.MASTER_RESPONSE)));
                                else if (myGroupTable.hasEntry(plugTracker.get(received_msg.getHeader().getSource())))
                                    cm_i.sendUnicast(new Message(
                                            new Header(received_msg.getHeader().getSource(),
                                                    plugTracker.get(received_msg.getHeader().getSource())),
                                            new Payload<>(IOTLanguage.TRACK_DEVICE)));
                                else plugTracker.remove(received_msg.getHeader().getSource());

                            } else {
                                String memberAddress = getAnAddress();
                                plugTracker.put(received_msg.getHeader().getSource(), memberAddress);
                                if (memberAddress.equals(myIP))
                                /*TODO: Add continue to if clause in order to skip master responses from group leader*/
                                    cm_i.sendUnicast(new Message(
                                            new Header(myIP, received_msg.getHeader().getSource()),
                                            new Payload<>(IOTLanguage.MASTER_RESPONSE)));
                                else
                                    cm_i.sendUnicast(new Message(
                                            new Header(received_msg.getHeader().getSource(),
                                                    memberAddress),
                                            new Payload<>(IOTLanguage.TRACK_DEVICE)));
                            }

                        } else if (received_msg.getPayload().getContent().toString().contains(IOTLanguage.REPORTING.toString())) {
                            String[] plugData = received_msg.getPayload().getContent().toString().split(" ");
                            plugDataToRegistry(plugData, received_msg.getHeader().getSource());
                            cm_i.sendUnicast(new Message(
                                    new Header(myIP, received_msg.getHeader().getSource()),
                                    new Payload<>(IOTLanguage.ACKNOWLEDGE)));

                            /**/
                            cm_i.sendUnicast(new Message(
                                    new Header(myIP, Configurations.CLOUD_ADDRESS),
                                    new Payload<>(new FogToken(myID, myIP, state, myGroupTable.getLeaderID(), plugRegistry.size()))
                            ));
                            logStatus();
                        }
                    }
                    break;
                }

            }
            System.out.println("sending token...");

        }
        cm_i.sendUnicast(new Message(
                new Header(myIP, Configurations.CLOUD_ADDRESS),
                new Payload<>(new FogToken(myID, myIP, state, myGroupTable.getLeaderID(), plugRegistry.size()))
        ));
        logStatus();
    }

    private void plugDataToRegistry(String[] data, String source) {
        if (data.length < 4) {
            System.err.println("Receiving insufficient data from plug: " + source);
            return;
        }

        EnumMap<RegistryIndicators, String> entries = new EnumMap<>(RegistryIndicators.class);
        int i = 1;
        for (RegistryIndicators indicator : RegistryIndicators.values()) {
            entries.put(indicator, data[i]);
            i++;
        }
        this.plugRegistry.update(source, entries);




                       /*  String[] plugData = received_msg.getPayload().getContent().toString().substring(9).split(" ");
                            System.out.println("Substring: "+received_msg.getPayload().getContent().toString().substring(9));
                            System.out.println("Split: "+plugData.toString());
                            for(int i=0; i<plugData.length; i++){
                                System.out.printf("Plug Data[%d] = %s",i,plugData[i]);
                            }
                            /System.out.println("Received: ");*/
    }

    @Override
    public String toString() {
        return "Peer id " + myID + "\n" +
                "Peer state:" + state + "\n" +
                "Peer ip: " + myIP + "\n" +
                "\nMy Group Table" + "\n" + myGroupTable.toString() +
                "\nTracked Addresses By KeepAlive:" + "\n" + heartBeat.toString() +
                "\n\n\t\t#Communication Manager#\n" + cm_i.toString() + "" +
                "\n\n\t\t#Device Cluster#\n" + plugRegistry.toString() +
                "\n##### END OF STATUS #####\n\n";
    }

    private void logStatus() {
        try (FileWriter fw = new FileWriter("peer" + myID + "_log.txt", true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println(toString());
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    private String getAnAddress() {
        if (buffCount < myGroupTable.getIpAddresses().length) {
            return myGroupTable.getIpAddresses()[buffCount++];
        } else {
            buffCount = 0;
            return myGroupTable.getIpAddresses()[0];
        }
    }
}
