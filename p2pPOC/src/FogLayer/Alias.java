package FogLayer;

/**
 * Dev: mmachado on 15/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public enum Alias {
    NODE,
    PEER,
    COMMUNICATION_MANAGER, KeepAlive;
}
