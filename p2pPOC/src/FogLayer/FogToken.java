package FogLayer;

import FogLayer.Peer.PeerBehaviours.PeerStates;

import java.io.Serializable;

/**
 * Dev: mmachado on 20/04/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class FogToken implements Serializable {
    private final int tokenPR;
    private final int tokenGP;
    private final String tokenIP;
    private final int tokenID;
    private final PeerStates tokenST;

    public FogToken(int myID, String myIP, PeerStates state, int leaderID, int plugRegistry) {
        this.tokenID = myID;
        this.tokenIP = myIP;
        this.tokenGP = leaderID;
        this.tokenPR = plugRegistry;
        this.tokenST = state;
    }

    public PeerStates getTokenST() { return tokenST; }

    public int getTokenPR() {
        return tokenPR;
    }

    public int getTokenGP() {
        return tokenGP;
    }

    public String getTokenIP() {
        return tokenIP;
    }

    public int getTokenID() {
        return tokenID;
    }

    @Override
    public String toString(){
        return "Peer Info{ ID: "+tokenID+"; IP: "+tokenIP+"; GroupID: "+tokenGP+"; Status: "+tokenST.toString()+"; }"+
                "\nAmmount of attached plugs: "+tokenPR+"\n;";
    }
}
