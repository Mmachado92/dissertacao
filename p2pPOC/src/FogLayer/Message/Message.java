package FogLayer.Message;


import FogLayer.Message.Header.Header;
import FogLayer.Message.Payload.Payload;

import java.io.Serializable;

/**
 * Dev: mmachado on 13/11/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Message implements Serializable, MessageInterface {

    private Header header;

    private Payload payload;

    public Message(Header header, Payload payload) {
        this.header = header;
        this.payload = payload;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public Header getHeader() {
        return header;
    }

    @Override
    public Payload getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "Message: \n"
                + this.header.toString()
                + this.payload.toString();
    }

    @Override
    public boolean hasIdentifier() {
        return false;
    }

    @Override
    public int getIdentifier() {
        return -1;
    }

    @Override
    public int getReport() {
        return 0;
    }
}
