package FogLayer.Message.Header;

import java.io.Serializable;

/**
 * Dev: mmachado on 15/11/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Header implements Serializable {

    private String source;

    private String destination;

    public Header(String source, String destination) {
        this.source = source;
        this.destination = destination;
    }

    public String getDestination() {
        assert destination != null : "Accessing null Header (destination)";
        return destination;
    }

    public String getSource() {
        assert destination != null : "Accessing null Header (source)";
        return source;
    }

    public void setSource(String source) { this.source = source; }

    public void setDestination(String destination) { this.destination = destination; }

    public String toString() {
        return "Header Information\n"
                + "Source: " + this.source + "\n"
                + "Destination: " + this.destination;
    }
}
