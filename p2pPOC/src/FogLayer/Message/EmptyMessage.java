package FogLayer.Message;

import FogLayer.Message.Header.Header;
import FogLayer.Message.Payload.Payload;

import java.io.Serializable;

/**
 * Dev: mmachado on 22/02/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class EmptyMessage implements Serializable, MessageInterface {

    public EmptyMessage() {
    }

    public Header getHeader() {
        return new Header("EmptySource", "EmptyPayload");
    }

    @Override
    public Payload getPayload() {
        return new Payload("EmptyPayload");
    }

    @Override
    public String toString() {
        return "Message: \n"
                + getHeader().toString()
                + getPayload().toString();
    }

    @Override
    public boolean hasIdentifier() {
        return false;
    }

    @Override
    public int getIdentifier() {
        return -1;
    }

    @Override
    public int getReport() {
        return 0;
    }
}