package FogLayer.Message;

import FogLayer.Message.Header.Header;
import FogLayer.Message.Payload.Payload;

import java.io.Serializable;

/**
 * Dev: mmachado on 08/03/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class IdentifierMessage implements Serializable, MessageInterface {

    private Integer senderID;
    private Header h;
    private Payload p;

    public IdentifierMessage(Header header, Payload payload, Integer id) {
        this.h = header;
        this.p = payload;
        this.senderID = id;
    }


    @Override
    public Header getHeader() {
        return h;
    }

    @Override
    public Payload getPayload() {
        return p;
    }

    public boolean hasIdentifier() { return true; }

    @Override
    public int getIdentifier() {
        return senderID;
    }

    @Override
    public int getReport() {
        return 0;
    }

    @Override
    public String toString() {
        return "Message: \n"
                + this.h.toString()+"\n"
                + this.p.toString()+"\n"
                + "ID:" +this.senderID;
    }
}
