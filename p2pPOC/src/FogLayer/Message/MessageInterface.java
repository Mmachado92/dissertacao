package FogLayer.Message;

import FogLayer.Message.Header.Header;
import FogLayer.Message.Payload.Payload;

import java.io.Serializable;

/**
 * Dev: mmachado on 15/11/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public interface MessageInterface extends Serializable {

    Header getHeader();

    Payload getPayload();

    String toString();

    boolean hasIdentifier();

    int getIdentifier();

    int getReport();
}
