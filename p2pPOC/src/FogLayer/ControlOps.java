package FogLayer;

import java.io.Serializable;

/**
 * Dev: mmachado on 20/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
@Deprecated
public enum ControlOps implements Serializable {
    JOIN,
    BULLY_ELECTION,
    LEAVE_GROUP;
}
