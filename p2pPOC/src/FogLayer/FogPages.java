package FogLayer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Dev: mmachado on 06/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
@SuppressWarnings("Duplicates")
public class FogPages {

    static File ipList;

    public FogPages() {
        File dir = new File(".");
        try {
            ipList = new File(dir.getCanonicalPath() + File.separator + "FogLayer/iplist");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*public String [] getBroadcastList(String requesterIP){

        ArrayList<String> al = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ipList))) {
            String line;
            while ((line = br.readLine()) != null) {
                if(requesterIP.equals(line.split(" ")[1])) continue;
                else al.add(line.split(" ")[1]);


            }
        } catch (IOException e) {
            System.err.println("Exception was caught: " + e.getMessage());
            System.err.println("Couldn't finish file parsing correctly");
        }

        //First Step: convert ArrayList to an Object array.
        Object[] objDays = al.toArray();

        //Second Step: convert Object array to String array
        return Arrays.copyOf(objDays, objDays.length, String[].class);
    }*/


    public String[] getBroadcastList(String requesterIP) {
        String[] adds = {
                "fe80:0:0:0:6a8a:b5ff:fe00:2228",
                "fe80:0:0:0:6a8a:b5ff:fe00:5f7",
                "fe80:0:0:0:6a8a:b5ff:fe00:22af",
                "fe80:0:0:0:6a8b:b5ff:fe00:849",
                "fe80:0:0:0:6a8a:b5ff:fe00:2225"

        };
        ArrayList<String> adds2 = new ArrayList<>();

        for (String add : adds) {
            if (!requesterIP.equals(add)) adds2.add(add);
        }
        String[] tmp = new String[adds2.size()];
        adds2.toArray(tmp);
        System.out.println("Returning "+tmp.length+" addresses");
        return tmp;
    }

    /*
    public String [] getBroadcastList(){

        ArrayList<String> al = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(ipList))) {
            String line;
            while ((line = br.readLine()) != null) {
                al.add(line.split(" ")[1]);
            }
        } catch (IOException e) {
            System.err.println("Exception was caught: " + e.getMessage());
            System.err.println("Couldn't finish file parsing correctly");
        }

        //First Step: convert ArrayList to an Object array.
        Object[] objDays = al.toArray();

        //Second Step: convert Object array to String array
        return Arrays.copyOf(objDays, objDays.length, String[].class);
    }*/
}
