package FogLayer;


import FogLayer.Peer.CommunicationManager.CommunicationManager;
import FogLayer.Peer.CommunicationManager.Manager;
import FogLayer.Peer.Peer;

/**
 * Dev: mmachado on 12/12/16.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Node {

    private Thread myPeer;
    private CommunicationManager cm_i;

    public Node(int peerID, String myIP) {
        this.cm_i = new Manager(myIP);
        this.myPeer = new Peer(myIP, peerID, cm_i);
        myPeer.start();
    }


    public String reportStatus() {
        return myPeer.toString();
    }

}
