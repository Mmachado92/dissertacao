package Cloud;

import FogLayer.FogToken;
import FogLayer.Message.MessageInterface;
import FogLayer.Peer.CommunicationManager.CommunicationManager;
import FogLayer.Peer.CommunicationManager.Manager;
import Tools.Configurations;

/**
 * Dev: mmachado on 19/04/17.
 * Project Name: p2pPOC
 * IDE: IntelliJ IDEA
 */
public class Morpheus extends Thread {
    //Behold, the master
    private SharedHashMap<Integer, FogToken> universeTracker;
    private CommunicationManager cm_i;
    private volatile boolean hasToRun;


    public Morpheus() {
        this.universeTracker = new SharedHashMap<>();
        this.cm_i = new Manager(Configurations.getInterfaceIP("ipv4", "wlp3s0"));
        this.hasToRun = true;
        this.setName("Morpheus Server");
    }

    public void interrupt() {
        this.hasToRun = false;
    }

    @Override
    public void run() {

        while (hasToRun) {
            MessageInterface receivedMessage = cm_i.receiveNextMessage();
            if (receivedMessage.getPayload().getContent() instanceof FogToken) {
                FogToken tmpToken = (FogToken) receivedMessage.getPayload().getContent();
                universeTracker.put(tmpToken.getTokenID(), tmpToken);
            }
        }
    }

    public void printUniverseStatus() {

        System.out.println(universeTracker.toString());
    }

    public void peerStatus(int id) {
        System.out.println("Peeking peer status...");

        if (universeTracker.containsKey(id))
            System.out.println(universeTracker.get(id).toString());
        else
            System.out.println("Couldn't find the bloke sir. No token received");
    }


}
